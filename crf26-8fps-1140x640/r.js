org.cmucreatelab.loadVideoset({
  "level_info": [
    {
      "rows": 1,
      "cols": 1
    },
    {
      "rows": 3,
      "cols": 2
    },
    {
      "rows": 9,
      "cols": 6
    },
    {
      "rows": 21,
      "cols": 14
    },
    {
      "rows": 45,
      "cols": 31
    },
    {
      "rows": 92,
      "cols": 64
    }
  ],
  "nlevels": 6,
  "level_scale": 2,
  "frames": 31,
  "fps": 8.0,
  "leader": 0,
  "tile_width": 286,
  "tile_height": 160,
  "video_width": 1140,
  "video_height": 640,
  "width": 18944,
  "height": 15173
})